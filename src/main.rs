#![feature(test)]
extern crate test;

extern crate image;
extern crate obj;
extern crate rand;

use obj::{Obj, SimplePolygon};
use std::path::Path;

mod utils;
use utils::*;

mod rasterizer;

fn main() {
    lesson2_homework();
}

fn lesson2_homework() {
    let obj = Obj::<SimplePolygon>::load(&Path::new("assets/african_head.obj")).unwrap();
    let mut diffuse_map = Img::from_file("assets/african_head_diffuse.png");
    diffuse_map.flip_verticaly();

    let mut img = Img::new(800, 800);
    img.fill(COLOR_BLACK);

    let light_dir = [0f32, 0f32, -1f32];

    rasterizer::draw_obj_flat_zbuffer_diffuse(&obj, &mut img, &diffuse_map, &light_dir);

    img.flip_verticaly();
    img.save("out.png");
}

fn lesson2_part2() {
    let obj = Obj::<SimplePolygon>::load(&Path::new("assets/african_head.obj")).unwrap();
    let mut img = Img::new(800, 800);
    img.fill(COLOR_BLACK);

    //rasterizer::draw_obj_random_fill(&obj, &mut img);
    let light_dir = [0f32, 0f32, -1f32];
    rasterizer::draw_obj_flat_zbuffer_1(&obj, &mut img, COLOR_WHITE, &light_dir);

    img.flip_verticaly();
    img.save("out.png");
}

fn lesson2_part1() {
    let obj = Obj::<SimplePolygon>::load(&Path::new("assets/african_head.obj")).unwrap();
    let mut img = Img::new(200, 200);
    img.fill(COLOR_BLACK);

    let t0 = [[10, 70], [50, 160], [70, 80]];
    let t1 = [[180, 50], [150, 1], [70, 180]];
    let t2 = [[180, 150], [120, 160], [130, 180]];
    let t3 = [[10, 10], [100, 30], [190, 160]];

    rasterizer::triangle(
        &mut img,
        &t0[0],
        &t0[1],
        &t0[2],
        COLOR_RED,
        COLOR_WHITE,
        COLOR_BLUE,
        true,
    );
    rasterizer::triangle(
        &mut img,
        &t1[0],
        &t1[1],
        &t1[2],
        COLOR_WHITE,
        COLOR_GREEN,
        COLOR_RED,
        true,
    );
    rasterizer::triangle(
        &mut img,
        &t2[0],
        &t2[1],
        &t2[2],
        COLOR_GREEN,
        COLOR_BLUE,
        COLOR_RED,
        true,
    );

    // filled
    rasterizer::triangle(
        &mut img,
        &t3[0],
        &t3[1],
        &t3[2],
        COLOR_RED,
        COLOR_GREEN,
        COLOR_BLUE,
        true,
    );

    // wired
    rasterizer::triangle(
        &mut img,
        &t3[0],
        &t3[1],
        &t3[2],
        COLOR_GREEN,
        COLOR_GREEN,
        COLOR_GREEN,
        false,
    );

    // triangle bbox
    let (min_x, min_y, max_x, max_y) = Math::triangle_bbox(&t3[0], &t3[1], &t3[2]);

    rasterizer::draw_box(
        &mut img,
        &[min_x, min_y],
        &[max_x, max_y],
        COLOR_WHITE,
        false,
    );

    img.flip_verticaly();
    img.save("out.png");
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_draw_line(bench: &mut Bencher) {
        let mut img = Img::new(100, 100);
        bench.iter(|| rasterizer::line(&mut img, 13, 20, 80, 40, COLOR_WHITE));
    }
}
