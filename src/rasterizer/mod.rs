use image::Rgba;
use obj::{Obj, SimplePolygon};
use rand::prelude::*;
use std::mem;
use std::{f32, i32};
use utils::*;

#[allow(dead_code)]
pub fn line2(img: &mut Img, v0: &Vec2, v1: &Vec2, color: Rgba<u8>) {
    line(img, v0[0], v0[1], v1[0], v1[1], color)
}

#[allow(dead_code)]
pub fn line(img: &mut Img, x0: i32, y0: i32, x1: i32, y1: i32, color: Rgba<u8>) {
    let mut steep = false;
    let mut x0 = x0;
    let mut y0 = y0;
    let mut x1 = x1;
    let mut y1 = y1;

    if (x0 - x1).abs() < (y0 - y1).abs() {
        mem::swap(&mut x0, &mut y0);
        mem::swap(&mut x1, &mut y1);
        steep = true;
    }

    if x0 > x1 {
        mem::swap(&mut x0, &mut x1);
        mem::swap(&mut y0, &mut y1);
    }

    let dx = x1 - x0;
    let dy = y1 - y0;
    let derror2 = dy.abs() * 2;
    let mut error2 = 0;
    let mut y = y0;

    for x in x0..x1 + 1 {
        if steep {
            img.set(y, x, color);
        } else {
            img.set(x, y, color);
        }

        error2 += derror2;
        if error2 > dx {
            y += if y1 > y0 { 1 } else { -1 };
            error2 -= dx * 2;
        }
    }
}

#[allow(dead_code)]
pub fn horizontal_line(img: &mut Img, x0: i32, x1: i32, y: i32, color: Rgba<u8>) {
    let mut x0 = x0;
    let mut x1 = x1;

    if x0 > x1 {
        mem::swap(&mut x0, &mut x1);
    }

    for x in x0..x1 {
        img.set(x, y, color);
    }
}

#[allow(dead_code)]
pub fn vertical_line(img: &mut Img, y0: i32, y1: i32, x: i32, color: Rgba<u8>) {
    let mut y0 = y0;
    let mut y1 = y1;

    if y0 > y1 {
        mem::swap(&mut y0, &mut y1);
    }

    for y in y0..y1 {
        img.set(x, y, color);
    }
}

#[allow(dead_code)]
pub fn draw_box(img: &mut Img, v0: &Vec2, v1: &Vec2, color: Rgba<u8>, fill: bool) {
    if fill {
        return filled_box(img, v0, v1, color);
    }

    // draw four lines
    horizontal_line(img, v0[0], v1[0], v0[1], color);
    vertical_line(img, v0[1], v1[1], v0[0], color);
    horizontal_line(img, v0[0], v1[0], v1[1], color);
    vertical_line(img, v0[1], v1[1], v1[0], color);
}

#[allow(dead_code)]
pub fn filled_box(img: &mut Img, v0: &Vec2, v1: &Vec2, color: Rgba<u8>) {
    for x in v0[0]..v1[0] {
        for y in v0[1]..v1[1] {
            img.set(x, y, color);
        }
    }
}

#[allow(dead_code)]
pub fn draw_obj(model: &Obj<SimplePolygon>, img: &mut Img, color: Rgba<u8>) {
    let width = img.get_width() as f32;
    let height = img.get_height() as f32;

    for o in &model.objects {
        for g in &o.groups {
            for face in &g.polys {
                for j in 0..3 {
                    let v0 = model.position[face[j].0];
                    let v1 = model.position[face[(j + 1) % 3].0];

                    let x0 = (v0[0] + 1f32) * width / 2f32;
                    let y0 = (v0[1] + 1f32) * height / 2f32;
                    let x1 = (v1[0] + 1f32) * width / 2f32;
                    let y1 = (v1[1] + 1f32) * height / 2f32;

                    line(img, x0 as i32, y0 as i32, x1 as i32, y1 as i32, color);
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn draw_obj_random_fill(model: &Obj<SimplePolygon>, img: &mut Img) {
    let width = img.get_width() as f32;
    let height = img.get_height() as f32;

    let mut color = COLOR_BLACK;

    for o in &model.objects {
        for g in &o.groups {
            for face in &g.polys {
                let mut t = [[0i32; 2]; 3];

                color.data[0] = thread_rng().gen_range(0, 255);
                color.data[1] = thread_rng().gen_range(0, 255);
                color.data[2] = thread_rng().gen_range(0, 255);

                for j in 0..3 {
                    let v = model.position[face[j].0];

                    let x = (v[0] + 1f32) * width / 2f32;
                    let y = (v[1] + 1f32) * height / 2f32;

                    t[j] = [x as i32, y as i32];
                }

                triangle_barycentric(img, &t[0], &t[1], &t[2], color, color, color);
            }
        }
    }
}

#[allow(dead_code)]
pub fn draw_obj_flat_1(
    model: &Obj<SimplePolygon>,
    img: &mut Img,
    color: Rgba<u8>,
    light_dir: &Vec3f,
) {
    let width = img.get_width() as f32;
    let height = img.get_height() as f32;

    for o in &model.objects {
        for g in &o.groups {
            for face in &g.polys {
                let mut t = [[0i32; 2]; 3];
                let mut w = [[0f32; 3]; 3];

                for j in 0..3 {
                    w[j] = model.position[face[j].0];

                    let v = &w[j];
                    let x = (v[0] + 1f32) * width / 2f32;
                    let y = (v[1] + 1f32) * height / 2f32;

                    t[j] = [x as i32, y as i32];
                }

                let n = Math::cross_product_3f(
                    &Math::substract_f32(&w[2], &w[0]),
                    &Math::substract_f32(&w[1], &w[0]),
                );
                let n = Math::normalize_f32(&n);

                let intensity = Math::scalar_mul(&n, light_dir);
                let mut c = ColorUtils::mul(intensity, color);
                c[3] = color[3];

                if intensity > 0f32 {
                    triangle_barycentric(img, &t[0], &t[1], &t[2], c, c, c);
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn draw_obj_flat_zbuffer_1(
    model: &Obj<SimplePolygon>,
    img: &mut Img,
    color: Rgba<u8>,
    light_dir: &Vec3f,
) {
    let width = img.get_width() as f32;
    let height = img.get_height() as f32;
    let mut zbuffer = vec![-f32::MAX; (img.get_width() * img.get_height()) as usize];

    for o in &model.objects {
        for g in &o.groups {
            for face in &g.polys {
                let mut t = [[0i32; 3]; 3];
                let mut w = [[0f32; 3]; 3];

                for j in 0..3 {
                    w[j] = model.position[face[j].0];

                    let v = &w[j];
                    let x = (v[0] + 1f32) * width / 2f32;
                    let y = (v[1] + 1f32) * height / 2f32;

                    t[j] = [x as i32, y as i32, v[2] as i32];
                }

                let n = Math::cross_product_3f(
                    &Math::substract_f32(&w[2], &w[0]),
                    &Math::substract_f32(&w[1], &w[0]),
                );
                let n = Math::normalize_f32(&n);

                let intensity = Math::scalar_mul(&n, light_dir);
                let mut c = ColorUtils::mul(intensity, color);
                c[3] = color[3];

                if intensity > 0f32 {
                    triangle_barycentric_3_zbuffer(img, &t[0], &t[1], &t[2], &mut zbuffer, c, c, c);
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn draw_obj_flat_zbuffer_diffuse(
    model: &Obj<SimplePolygon>,
    img: &mut Img,
    diffuse_map: &Img,
    light_dir: &Vec3f,
) {
    let width = img.get_width() as f32;
    let height = img.get_height() as f32;
    let mut zbuffer = vec![-f32::MAX; (img.get_width() * img.get_height()) as usize];

    for o in &model.objects {
        for g in &o.groups {
            for face in &g.polys {
                let mut t = [[0i32; 3]; 3];
                let mut w = [[0f32; 3]; 3];
                let mut uv = [[0f32; 2]; 3];

                for j in 0..3 {
                    w[j] = model.position[face[j].0];
                    uv[j] = model.texture[face[j].1.unwrap()];

                    let v = &w[j];
                    let x = (v[0] + 1f32) * width / 2f32;
                    let y = (v[1] + 1f32) * height / 2f32;

                    t[j] = [x as i32, y as i32, v[2] as i32];
                }

                let n = Math::cross_product_3f(
                    &Math::substract_f32(&w[2], &w[0]),
                    &Math::substract_f32(&w[1], &w[0]),
                );
                let n = Math::normalize_f32(&n);

                let intensity = Math::scalar_mul(&n, light_dir);
                // let mut c = ColorUtils::mul(intensity, color);
                // c[3] = color[3];

                if intensity > 0f32 {
                    triangle_barycentric_3_zbuffer_diffuse(
                        img,
                        diffuse_map,
                        &t[0],
                        &t[1],
                        &t[2],
                        &mut zbuffer,
                        &uv,
                        intensity,
                    );
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn draw_obj_flat_zbuffer(
    model: &Obj<SimplePolygon>,
    img: &mut Img,
    color: Rgba<u8>,
    light_dir: &Vec3f,
) {
    let width = img.get_width() as f32;
    let height = img.get_height() as f32;
    let mut zbuffer = vec![-f32::MAX; (img.get_width() * img.get_width()) as usize];

    for o in &model.objects {
        for g in &o.groups {
            for face in &g.polys {
                let mut t = [[0f32; 3]; 3];
                let mut w = [[0f32; 3]; 3];

                for j in 0..3 {
                    w[j] = model.position[face[j].0];
                    let v = &w[j];

                    let x = (v[0] + 1f32) * width / 2f32 + 0.5f32;
                    let y = (v[1] + 1f32) * height / 2f32 + 0.5f32;

                    t[j] = [x, y, v[2]];
                }

                let n = Math::cross_product_3f(
                    &Math::substract_f32(&w[2], &w[0]),
                    &Math::substract_f32(&w[1], &w[0]),
                );
                let n = Math::normalize_f32(&n);

                let intensity = Math::scalar_mul(&n, light_dir);
                let mut c = ColorUtils::mul(intensity, color);
                c[3] = color[3];

                if intensity > 0f32 {
                    triangle_barycentric_3f_zbuffer(
                        img,
                        &t[0],
                        &t[1],
                        &t[2],
                        &mut zbuffer,
                        c,
                        c,
                        c,
                    );
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn draw_obj_flat_2(
    model: &Obj<SimplePolygon>,
    img: &mut Img,
    color: Rgba<u8>,
    light_dir: &Vec3f,
) {
    let width = img.get_width() as f32;
    let height = img.get_height() as f32;

    for o in &model.objects {
        for g in &o.groups {
            for face in &g.polys {
                let mut t = [[0f32; 3]; 3];
                let mut w = [[0f32; 3]; 3];

                for j in 0..3 {
                    w[j] = model.position[face[j].0];
                    let v = &w[j];

                    let x = (v[0] + 1f32) * width / 2f32;
                    let y = (v[1] + 1f32) * height / 2f32;

                    t[j] = [x, y, v[2]];
                }

                // let n = Math::cross_product_3f(
                //     &Math::substract_f32(&w[2], &w[0]),
                //     &Math::substract_f32(&w[1], &w[0]),
                // );
                // let n = Math::normalize_f32(&n);

                // let intensity = Math::scalar_mul(&n, light_dir);
                // let mut c = ColorUtils::mul(intensity, color);
                // c[3] = color[3];

                // if intensity > 0f32 {
                //     triangle_barycentric_3f(img, &t[0], &t[1], &t[2], c, c, c);
                // }

                triangle_barycentric_3f(img, &t[0], &t[1], &t[2], color, color, color);
            }
        }
    }
}

#[allow(dead_code)]
pub fn triangle(
    img: &mut Img,
    v0: &Vec2,
    v1: &Vec2,
    v2: &Vec2,
    c0: Rgba<u8>,
    c1: Rgba<u8>,
    c2: Rgba<u8>,
    fill: bool,
) {
    if fill {
        // return triangle_fill(img, v0, v1, v2, c0, c1, c2);
        return triangle_barycentric(img, v0, v1, v2, c0, c1, c2);
    }

    line2(img, &v0, &v1, c0);
    line2(img, &v1, &v2, c0);
    line2(img, &v2, &v0, c0);
}

#[allow(dead_code)]
pub fn triangle_fill(
    img: &mut Img,
    v0: Vec2,
    v1: Vec2,
    v2: Vec2,
    c0: Rgba<u8>,
    c1: Rgba<u8>,
    c2: Rgba<u8>,
) {
    let mut v0 = v0;
    let mut v1 = v1;
    let mut v2 = v2;

    // sort vertices by Y
    if v0[1] > v1[1] {
        mem::swap(&mut v0, &mut v1);
    }

    if v0[1] > v2[1] {
        mem::swap(&mut v0, &mut v2);
    }

    if v1[1] > v2[1] {
        mem::swap(&mut v1, &mut v2);
    }

    for y in v0[1]..v1[1] {
        let x1 = v0[0] + (v1[0] - v0[0]) * (y - v0[1]) / (v1[1] - v0[1]);
        let x2 = v0[0] + (v2[0] - v0[0]) * (y - v0[1]) / (v2[1] - v0[1]);

        horizontal_line(img, x1, x2, y, c0);
    }

    for y in v1[0]..v2[1] {
        let x1 = v1[0] + (v2[0] - v1[0]) * (y - v1[1]) / (v2[1] - v1[1]);
        let x2 = v0[0] + (v2[0] - v0[0]) * (y - v0[1]) / (v2[1] - v0[1]);

        horizontal_line(img, x1, x2, y, c0);
    }
}

#[allow(dead_code)]
pub fn triangle_barycentric(
    img: &mut Img,
    v0: &Vec2,
    v1: &Vec2,
    v2: &Vec2,
    c0: Rgba<u8>,
    c1: Rgba<u8>,
    c2: Rgba<u8>,
) {
    // calculate bounding box
    let (min_x, min_y, max_x, max_y) = Math::triangle_bbox(&v0, &v1, &v2);

    for y in min_y..max_y {
        for x in min_x..max_x {
            let (a, b, c) = Math::barycentric(&v0, &v1, &v2, x, y);

            if (b >= 0f32) && (c >= 0f32) && (b + c <= 1f32) {
                let color = ColorUtils::add(
                    ColorUtils::add(ColorUtils::mul(a, c0), ColorUtils::mul(b, c1)),
                    ColorUtils::mul(c, c2),
                );
                img.set(x, y, color);
            }
        }
    }
}

#[allow(dead_code)]
pub fn triangle_barycentric_3f(
    img: &mut Img,
    v0: &Vec3f,
    v1: &Vec3f,
    v2: &Vec3f,
    c0: Rgba<u8>,
    c1: Rgba<u8>,
    c2: Rgba<u8>,
) {
    // calculate bounding box
    let (min_x, min_y, max_x, max_y) = Math::triangle_bbox_3f_2d(&v0, &v1, &v2);
    let width = img.get_width();

    for y in min_y..max_y {
        for x in min_x..max_x {
            //let (a, b, c) = Math::barycentric_3f(&v0, &v1, &v2, x as f32, y as f32);
            let (a, b, c) = Math::barycentric_3f_2d(&v0, &v1, &v2, x, y);

            if (b >= 0f32) && (c >= 0f32) && (b + c <= 1f32) {
                let color = ColorUtils::add(
                    ColorUtils::add(ColorUtils::mul(a, c0), ColorUtils::mul(b, c1)),
                    ColorUtils::mul(c, c2),
                );

                img.set(x, y, color);
            }
        }
    }
}

#[allow(dead_code)]
pub fn triangle_barycentric_3(
    img: &mut Img,
    v0: &Vec3,
    v1: &Vec3,
    v2: &Vec3,
    c0: Rgba<u8>,
    c1: Rgba<u8>,
    c2: Rgba<u8>,
) {
    // calculate bounding box
    let (min_x, min_y, _, max_x, max_y, _) = Math::triangle_bbox_3(&v0, &v1, &v2);
    let width = img.get_width();

    for y in min_y..max_y {
        for x in min_x..max_x {
            //let (a, b, c) = Math::barycentric_3f(&v0, &v1, &v2, x as f32, y as f32);
            let (a, b, c) = Math::barycentric_3(&v0, &v1, &v2, x, y);

            if (b >= 0f32) && (c >= 0f32) && (b + c <= 1f32) {
                let color = ColorUtils::add(
                    ColorUtils::add(ColorUtils::mul(a, c0), ColorUtils::mul(b, c1)),
                    ColorUtils::mul(c, c2),
                );

                img.set(x, y, color);
            }
        }
    }
}

#[allow(dead_code)]
pub fn triangle_barycentric_3_zbuffer(
    img: &mut Img,
    v0: &Vec3,
    v1: &Vec3,
    v2: &Vec3,
    zbuffer: &mut [f32],
    c0: Rgba<u8>,
    c1: Rgba<u8>,
    c2: Rgba<u8>,
) {
    // calculate bounding box
    let (min_x, min_y, _, max_x, max_y, _) = Math::triangle_bbox_3(&v0, &v1, &v2);
    let width = img.get_width();

    for y in min_y..max_y {
        for x in min_x..max_x {
            //let (a, b, c) = Math::barycentric_3f(&v0, &v1, &v2, x as f32, y as f32);
            let (a, b, c) = Math::barycentric_3(&v0, &v1, &v2, x, y);

            if (b >= 0f32) && (c >= 0f32) && (b + c <= 1f32) {
                let color = ColorUtils::add(
                    ColorUtils::add(ColorUtils::mul(a, c0), ColorUtils::mul(b, c1)),
                    ColorUtils::mul(c, c2),
                );

                let mut z = 0f32;

                z += v0[2] as f32 * a;
                z += v1[2] as f32 * b;
                z += v2[2] as f32 * c;

                if zbuffer[(x + y * width) as usize] < z {
                    zbuffer[(x + y * width) as usize] = z;
                    img.set(x, y, color);
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn triangle_barycentric_3_zbuffer_diffuse(
    img: &mut Img,
    diffuse_map: &Img,
    v0: &Vec3,
    v1: &Vec3,
    v2: &Vec3,
    zbuffer: &mut [f32],
    uv: &[[f32; 2]; 3],
    intensity: f32,
) {
    // calculate bounding box
    let (min_x, min_y, _, max_x, max_y, _) = Math::triangle_bbox_3(&v0, &v1, &v2);
    let width = img.get_width();

    let d_w = diffuse_map.get_width() as f32;
    let d_h = diffuse_map.get_height() as f32;

    for y in min_y..max_y {
        for x in min_x..max_x {
            //let (a, b, c) = Math::barycentric_3f(&v0, &v1, &v2, x as f32, y as f32);
            let (a, b, c) = Math::barycentric_3(&v0, &v1, &v2, x, y);

            if (b >= 0f32) && (c >= 0f32) && (b + c <= 1f32) {
                let a_uv: Vec2f = Math::mul_2f(a, &uv[0]);
                let b_uv: Vec2f = Math::mul_2f(b, &uv[1]);
                let c_uv: Vec2f = Math::mul_2f(c, &uv[2]);

                let result_uv = Math::add_2f(&Math::add_2f(&a_uv, &b_uv), &c_uv);
                let color =
                    diffuse_map.get((d_w * result_uv[0]) as i32, (d_h * result_uv[1]) as i32);

                let mut result_color = ColorUtils::mul(intensity, color);
                result_color[3] = color[3];

                let mut z = 0f32;

                z += v0[2] as f32 * a;
                z += v1[2] as f32 * b;
                z += v2[2] as f32 * c;

                if zbuffer[(x + y * width) as usize] < z {
                    zbuffer[(x + y * width) as usize] = z;
                    img.set(x, y, result_color);
                }
            }
        }
    }
}

#[allow(dead_code)]
pub fn triangle_barycentric_3f_zbuffer(
    img: &mut Img,
    v0: &Vec3f,
    v1: &Vec3f,
    v2: &Vec3f,
    zbuffer: &mut [f32],
    c0: Rgba<u8>,
    c1: Rgba<u8>,
    c2: Rgba<u8>,
) {
    // calculate bounding box
    let (min_x, min_y, max_x, max_y) = Math::triangle_bbox_3f_2d(&v0, &v1, &v2);
    let width = img.get_width();

    for y in min_y..max_y {
        for x in min_x..max_x {
            // let (a, b, c) = Math::barycentric_3f(&v0, &v1, &v2, x as f32, y as f32);
            let (a, b, c) = Math::barycentric_3f_2d(&v0, &v1, &v2, x, y);

            if a < 0f32 || b < 0f32 || c < 0f32 {
                continue;
            }

            let color = ColorUtils::add(
                ColorUtils::add(ColorUtils::mul(a, c0), ColorUtils::mul(b, c1)),
                ColorUtils::mul(c, c2),
            );

            let mut z = 0f32;

            z += v0[2] * a;
            z += v1[2] * b;
            z += v2[2] * c;

            if zbuffer[(x + y * width) as usize] < z {
                zbuffer[(x + y * width) as usize] = z;
                img.set(x, y, color);
            }
        }
    }
}
