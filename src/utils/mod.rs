use image::{imageops, open, Rgba, RgbaImage};
use std::path::Path;

pub const COLOR_ZERO: Rgba<u8> = Rgba { data: [0; 4] };
pub const COLOR_WHITE: Rgba<u8> = Rgba { data: [255; 4] };
pub const COLOR_BLACK: Rgba<u8> = Rgba {
    data: [0, 0, 0, 255],
};
pub const COLOR_RED: Rgba<u8> = Rgba {
    data: [255, 0, 0, 255],
};
pub const COLOR_GREEN: Rgba<u8> = Rgba {
    data: [0, 255, 0, 255],
};
pub const COLOR_BLUE: Rgba<u8> = Rgba {
    data: [0, 0, 255, 255],
};

pub type Vec2 = [i32; 2];
pub type Vec3 = [i32; 3];

pub type Vec2f = [f32; 2];
pub type Vec3f = [f32; 3];

pub struct Img {
    img: RgbaImage,
    w: i32,
    h: i32,
}

impl Img {
    pub fn new(width: u32, height: u32) -> Img {
        let img = RgbaImage::new(width, height);

        Img {
            img: img,
            w: width as i32,
            h: height as i32,
        }
    }

    pub fn from_file(file_name: &str) -> Img {
        let path = Path::new(file_name);
        let img: RgbaImage = open(path).unwrap().to_rgba();

        let w = img.width() as i32;
        let h = img.height() as i32;

        Img {
            img: img,
            w: w,
            h: h,
        }
    }

    pub fn get_width(&self) -> i32 {
        self.w
    }

    pub fn get_height(&self) -> i32 {
        self.h
    }

    pub fn set(&mut self, x: i32, y: i32, color: Rgba<u8>) -> bool {
        if x < 0 || y < 0 || x >= self.w || y >= self.h {
            return false;
        }

        self.img.put_pixel(x as u32, y as u32, color);

        true
    }

    pub fn fill(&mut self, color: Rgba<u8>) -> bool {
        for y in 0..self.h {
            for x in 0..self.w {
                self.set(x, y, color);
            }
        }

        true
    }

    #[allow(dead_code)]
    pub fn get(&self, x: i32, y: i32) -> Rgba<u8> {
        if x < 0 || y < 0 || x >= self.w || y >= self.h {
            return COLOR_ZERO;
        }

        self.img.get_pixel(x as u32, y as u32).clone()
    }

    pub fn flip_verticaly(&mut self) -> bool {
        self.img = imageops::flip_vertical(&self.img);

        true
    }

    #[allow(dead_code)]
    pub fn flip_horizontaly(&mut self) -> bool {
        self.img = imageops::flip_horizontal(&self.img);

        true
    }

    pub fn save(&self, file_name: &str) {
        self.img.save(file_name).unwrap();
    }
}

pub struct ColorUtils {}

impl ColorUtils {
    #[allow(dead_code)]
    pub fn mul(x: f32, color: Rgba<u8>) -> Rgba<u8> {
        let mut c = color;
        c.data[0] = (x * c.data[0] as f32) as u8;
        c.data[1] = (x * c.data[1] as f32) as u8;
        c.data[2] = (x * c.data[2] as f32) as u8;
        c.data[3] = (x * c.data[3] as f32) as u8;

        c
    }

    #[allow(dead_code)]
    pub fn add(c1: Rgba<u8>, c2: Rgba<u8>) -> Rgba<u8> {
        let mut c = c1;
        c.data[0] += c2.data[0];
        c.data[1] += c2.data[1];
        c.data[2] += c2.data[2];
        c.data[3] += c2.data[3];

        c
    }
}

pub struct Math {}

impl Math {
    #[allow(dead_code)]
    pub fn cross_product(a: &Vec2, b: &Vec2) -> f32 {
        (a[0] * b[1] - a[1] * b[0]) as f32
    }

    #[allow(dead_code)]
    pub fn cross_product_3f(a: &Vec3f, b: &Vec3f) -> Vec3f {
        [
            (a[1] * b[2] - a[2] * b[1]),
            (a[2] * b[0] - a[0] * b[2]),
            (a[0] * b[1] - a[1] * b[0]),
        ]
    }

    #[allow(dead_code)]
    pub fn cross_product_3(a: &Vec3, b: &Vec3) -> Vec3 {
        [
            (a[1] * b[2] - a[2] * b[1]),
            (a[2] * b[0] - a[0] * b[2]),
            (a[0] * b[1] - a[1] * b[0]),
        ]
    }

    #[allow(dead_code)]
    pub fn scalar_mul(a: &Vec3f, b: &Vec3f) -> f32 {
        a[0] * b[0] + a[1] * b[1] + a[2] * b[2]
    }

    #[allow(dead_code)]
    pub fn substract_f32(a: &Vec3f, b: &Vec3f) -> Vec3f {
        [a[0] - b[0], a[1] - b[1], a[2] - b[2]]
    }

    #[allow(dead_code)]
    pub fn dot_product(v0: &Vec2, v1: &Vec2) -> i32 {
        (v0[0] * v1[0]) + (v0[1] * v1[1])
    }

    #[allow(dead_code)]
    pub fn dot_product_f32(v0: &Vec3f, v1: &Vec3f) -> f32 {
        (v0[0] * v1[0]) + (v0[1] * v1[1]) + (v0[2] * v1[2])
    }

    #[allow(dead_code)]
    pub fn norm_f32(v: &Vec3f) -> f32 {
        (v[0] * v[0] + v[1] * v[1] + v[2] * v[2]).sqrt()
    }

    #[allow(dead_code)]
    pub fn normalize_f32(v: &Vec3f) -> Vec3f {
        let n = 1f32 / Math::norm_f32(v);

        [v[0] * n, v[1] * n, v[2] * n]
    }

    #[allow(dead_code)]
    pub fn add_2f(v0: &Vec2f, v1: &Vec2f) -> Vec2f {
        [v0[0] + v1[0], v0[1] + v1[1]]
    }

    #[allow(dead_code)]
    pub fn mul_2f(a: f32, v: &Vec2f) -> Vec2f {
        [a * v[0], a * v[1]]
    }

    #[allow(dead_code)]
    pub fn triangle_bbox(v0: &Vec2, v1: &Vec2, v2: &Vec2) -> (i32, i32, i32, i32) {
        let mut min_x = v0[0];
        let mut min_y = v0[1];
        let mut max_x = v0[0];
        let mut max_y = v0[1];

        if min_x > v1[0] {
            min_x = v1[0]
        }
        if min_x > v2[0] {
            min_x = v2[0]
        }

        if min_y > v1[1] {
            min_y = v1[1]
        }
        if min_y > v2[1] {
            min_y = v2[1]
        }

        if max_x < v1[0] {
            max_x = v1[0]
        }
        if max_x < v2[0] {
            max_x = v2[0]
        }

        if max_y < v1[1] {
            max_y = v1[1]
        }
        if max_y < v2[1] {
            max_y = v2[1]
        }

        (min_x, min_y, max_x, max_y)
    }

    #[allow(dead_code)]
    pub fn triangle_bbox_3(v0: &Vec3, v1: &Vec3, v2: &Vec3) -> (i32, i32, i32, i32, i32, i32) {
        let mut min_x = v0[0];
        let mut min_y = v0[1];
        let mut min_z = v0[2];
        let mut max_x = v0[0];
        let mut max_y = v0[1];
        let mut max_z = v0[2];

        if min_x > v1[0] {
            min_x = v1[0]
        }
        if min_x > v2[0] {
            min_x = v2[0]
        }

        if min_y > v1[1] {
            min_y = v1[1]
        }
        if min_y > v2[1] {
            min_y = v2[1]
        }

        if min_z > v1[1] {
            min_z = v1[1]
        }
        if min_z > v2[1] {
            min_z = v2[1]
        }

        if max_x < v1[0] {
            max_x = v1[0]
        }
        if max_x < v2[0] {
            max_x = v2[0]
        }

        if max_y < v1[1] {
            max_y = v1[1]
        }
        if max_y < v2[1] {
            max_y = v2[1]
        }

        if max_z < v1[2] {
            max_z = v1[2]
        }
        if max_z < v2[2] {
            max_z = v2[2]
        }

        (min_x, min_y, min_z, max_x, max_y, max_z)
    }

    #[allow(dead_code)]
    pub fn triangle_bbox_3f_2d(v0: &Vec3f, v1: &Vec3f, v2: &Vec3f) -> (i32, i32, i32, i32) {
        let mut min_x = v0[0];
        let mut min_y = v0[1];
        let mut max_x = v0[0];
        let mut max_y = v0[1];

        if min_x > v1[0] {
            min_x = v1[0]
        }
        if min_x > v2[0] {
            min_x = v2[0]
        }

        if min_y > v1[1] {
            min_y = v1[1]
        }
        if min_y > v2[1] {
            min_y = v2[1]
        }

        if max_x < v1[0] {
            max_x = v1[0]
        }
        if max_x < v2[0] {
            max_x = v2[0]
        }

        if max_y < v1[1] {
            max_y = v1[1]
        }
        if max_y < v2[1] {
            max_y = v2[1]
        }

        (min_x as i32, min_y as i32, max_x as i32, max_y as i32)
    }

    #[allow(dead_code)]
    pub fn barycentric(v0: &Vec2, v1: &Vec2, v2: &Vec2, x: i32, y: i32) -> (f32, f32, f32) {
        let vs1 = [v1[0] - v0[0], v1[1] - v0[1]];
        let vs2 = [v2[0] - v0[0], v2[1] - v0[1]];

        let q1 = [x - v0[0], y - v0[1]];
        let q2 = [x - v1[0], y - v1[1]];

        let common_divider = Math::cross_product(&vs1, &vs2);
        let s = Math::cross_product(&q1, &vs2) / common_divider;
        let t = Math::cross_product(&vs1, &q2) / common_divider;

        (1f32 - s - t, s, t)
    }

    #[allow(dead_code)]
    pub fn barycentric_3f(v0: &Vec3f, v1: &Vec3f, v2: &Vec3f, x: f32, y: f32) -> (f32, f32, f32) {
        let s1 = [v2[1] - v0[1], v1[1] - v0[1], v0[1] - x];
        let s2 = [v2[0] - v0[0], v1[0] - v0[0], v0[0] - y];

        let u = Math::cross_product_3f(&s1, &s2);

        if u[2].abs() > 1e-2 {
            return (1f32 - (u[0] + u[1]) / u[2], u[1] / u[2], u[0] / u[2]);
        }

        (-1f32, 1f32, 1f32)
    }

    #[allow(dead_code)]
    pub fn barycentric_3f_2d(
        v0: &Vec3f,
        v1: &Vec3f,
        v2: &Vec3f,
        x: i32,
        y: i32,
    ) -> (f32, f32, f32) {
        let vs1 = [(v1[0] - v0[0]) as i32, (v1[1] - v0[1]) as i32];
        let vs2 = [(v2[0] - v0[0]) as i32, (v2[1] - v0[1]) as i32];

        let q1 = [x - v0[0] as i32, y - v0[1] as i32];
        let q2 = [x - v1[0] as i32, y - v1[1] as i32];

        let common_divider = Math::cross_product(&vs1, &vs2);
        let s = Math::cross_product(&q1, &vs2) / common_divider;
        let t = Math::cross_product(&vs1, &q2) / common_divider;

        (1f32 - s - t, s, t)
    }

    #[allow(dead_code)]
    pub fn barycentric_3(v0: &Vec3, v1: &Vec3, v2: &Vec3, x: i32, y: i32) -> (f32, f32, f32) {
        let vs1 = [(v1[0] - v0[0]), (v1[1] - v0[1])];
        let vs2 = [(v2[0] - v0[0]), (v2[1] - v0[1])];

        let q1 = [x - v0[0], y - v0[1]];
        let q2 = [x - v1[0], y - v1[1]];

        let common_divider = Math::cross_product(&vs1, &vs2);
        let s = Math::cross_product(&q1, &vs2) / common_divider;
        let t = Math::cross_product(&vs1, &q2) / common_divider;

        (1f32 - s - t, s, t)
    }

    #[allow(dead_code)]
    pub fn barycentric_3_2(v0: &Vec3, v1: &Vec3, v2: &Vec3, x: i32, y: i32) -> (f32, f32, f32) {
        let s1 = [v2[1] - v0[1], v1[1] - v0[1], v0[1] - x];
        let s2 = [v2[0] - v0[0], v1[0] - v0[0], v0[0] - y];

        let u = Math::cross_product_3(&s1, &s2);
        let uf = [u[0] as f32, u[1] as f32, u[2] as f32];

        if u[2].abs() > 0 {
            return (1f32 - (uf[0] + uf[1]) / uf[2], uf[1] / uf[2], uf[0] / uf[2]);
        }

        (-1f32, 1f32, 1f32)
    }
}
